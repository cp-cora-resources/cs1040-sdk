// source: cs1040-ul.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = (function() {
  if (this) { return this; }
  if (typeof window !== 'undefined') { return window; }
  if (typeof global !== 'undefined') { return global; }
  if (typeof self !== 'undefined') { return self; }
  return Function('return this')();
}.call(null));

var lowPowerDevice_pb = require('./lowPowerDevice_pb.js');
goog.object.extend(proto, lowPowerDevice_pb);
var motion_pb = require('./motion_pb.js');
goog.object.extend(proto, motion_pb);
var temperature_pb = require('./temperature_pb.js');
goog.object.extend(proto, temperature_pb);
goog.exportSymbol('proto.Cora.CS1040.ConfigurationMode', null, global);
/**
 * @enum {number}
 */
proto.Cora.CS1040.ConfigurationMode = {
  CLEAR: 0,
  CUSTOM: 0,
  IMPULSE: 1,
  CONTINUOUS: 2,
  GARAGE0: 3,
  GARAGE1: 4,
  GARAGE3: 5
};

goog.object.extend(exports, proto.Cora.CS1040);
