![Cora Logo](img/cora-logo.png)

# Untethered Tilt & Vibration Sensor SDK - CS1040-UL

Information and examples for integrating the CS1040-UL untethered LoRaWAN device.  

The CS1040 Tilt and Vibration sensor provides real time reporting of vibration and orientation related events.   The sensor implements Codepoint's standard untethered application protocol using Google Protocol Buffers (protobuf) to encode and decode the messages communicated between the device and host application.   The protobuf specifications can be found at
[./defs/protobuf](defs/protobuf/README.md).  Generated source code is provided for C++, java, Javascript (JS), and Python.  Use the protoc code generator to generate packages for other languages.   

## Functional Model

The logical interaction model for the CS1040-UL is shown below.

![Cora Untethered Protocol](img/CS1040-UL-Protocol.png)

The applications interact with the CS1040-UL using the Cora Untethered Device Protocol, which organizes interaction into functional services as defined 
in the protobuff specifications.  The services for the CS1040-UL are as follows:

- **Device Settings** – General device configuration and notification messages.
- **Motion Sensor** – Motion sensor related messages; the motion sensor is a configurable multi-state sensor detecting various motion and timing events. 
- **Temperature Sensors** – Temperature sensing messages supporting configuration, events, measurements and statistics.

The device provides the motion sensor (id=0) and a temperature sensor (id=0) (+- 2-degree Celsius accuracy), which can be configured to support
a wide variety of applications. The device is configured by default as a garage door sensor.   The user can use the button to configure one of six predefined operating modes:  general vibration,  continuous motion detection (start/stop), garage door (4 configs with various door-open reminder durations). The device will also send a health check notification every 3 hours.  For simple applications, no custom configuration is required.  

However, the device can be configured to do much more.  Using the configuration messages application providers can configure
the device to provide complex vibration detection schemes, periodic statistics, temperature data, and other notifications.   

## Application Integration

The CS1040-UL implements LoRaWAN v1.0.3, which is supported by a wide variety of LoRaWAN network providers.  As shown in the diagram 
below,  the device communicates with an application via the LoRaWAN network and gateways.   Typically, the application implements
an encoder/decoder to translate the proprietary device messges into a data structure compatible with the application.   

![CS1040 Encoder / Decoder](img/CS1040-UL-Encoder.png)

For CS-1040-UL devices, this can be accomplished by decoding uplinked messages or encoding downlink messages using the code generated protobuf code
to convert to/from the binary wireline encoding into message objects.   From there, the application provider can easily translate
the data into a format compatible with their system. Depending on the network provider, it may also be possible to implement the message
encoding and decoding functions at the network application server.  For example, The Things Network (TTN) has such capabilities.

The specific design and details for integrating the CS1040-UL are mostly dependent on the application architecture and out of scope for this SDK.  This
SDK provides examples on implementing encoding/decoding functions as well as providing various test cases and stimulus to accelerate
integration.

## CS1040-UL Messages
Programmer's have a variety of language options available to process CS-1040 messages.  Conceptually, each Protobuf service interaction defined in the .proto files
are mapped to a specific port on the CS1040.  While the defined interaction is not strictly RPC, it does conform with event, request/response semantics.  Where 
noted in the .proto specifications, events are outbound messages with only the request portion of the service interaction defined.  For savvy developers,
the defined interactions could be mapped easly to gRPC if desired. 

The following sub-sections define the LoRaWAN port mappings to the Protobuff interaction for each of the services implemented
in the device.

### [Device Settings (lowpower.proto)](defs/protobuf/lowpower.proto)
The Low Power service provides interactions related to general device health and configuration.  The table below shows the LoRaWAN port mappings for 
the specified interaction.  See the protobuff file for detailed descriptions of the messages used to communicate.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Device.)   | Description                           |
|:----------:|:-----------:|:----------------:|------------------|---------------------------------------|
|    0x8     |      8      |      Request     | SetDefinedConfig | Sets device application to a predefined configuration|
|    0x10    |      16     |       Event      | HealthCheckEvent | Periodic health check event           |
|    0x11    |      17     |       Event      | ResetEvent       | Device reset notification event       |
|    0x12    |      18     | Request/Response | GetConfig        | Gets the current Device configuration |
|    0x13    |      19     | Request/Response | SetConfig        | Sets the device configration          |


### [Motion Sensor (motion.proto)](defs/protobuf/motion.proto)
The CS1040-UL uses the motion sensor service interaction to report and configure the motion related information.  It is identifed
as motion sensor #0 (id=0).  Events and request/response interactions will all identify the motion 
sensor as id=0.  

The table below shows the LoRaWAN port mappings for the motion sensor service.  See the protobuff file for detailed descriptions 
of the messages used to communicate.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Motion.Sensor) | Description                            |
|:----------:|:-----------:|:----------------:|:----------------------:|------------------------------------------|
|    0x27    |      39     |       Event      |       NotifyEvent      | Notifies of a motion state change event.       |
|    0x28    |      40     |       Event      |    StatsReportEvent    | Stats reported at regular intervals.     |
|    0x29    |      41     | Request/Response |        GetSettings     | Gets the motion sensor configuration.   |
|    0x2A    |      42     | Request/Response |        SetSettings     | Sets the motion sensor configuration.   |
|    0x2B    |      43     | Request/Response |        GetStats        | Gets the current motion statistics.     |
|    0x2C    |      44     |      Request     |       StatsClear       | Updates the specified sensor item.       |
|    0x2D    |      45     | Request/Response |         SetItem        | Sets a motion sensor item (Trigger/State definition). |
|    0x2E    |      46     | Request/Response |         GetItem        | Get a specified motion sensor item (Trigger/State definition). |


To regularly report statistics, configure the 'statsInterval' to report at the desired interval.  Maximum report interval 
is 49 days (70,560 minutes).  Additionally, notification events and stat messages can be marked as confirmed to ensure delivery. 


#### CS1040 Standard Configuration Modes and Event Types
The CS1040 datasheet specifies 6 operating modes that can be selected by push-button or configured by the integrator via downlink (port 8).
The Event ID's field in the Notification Event uplink (Port 39) vary based on which mode the device is currently configured.  The Event Id's for the three pre-defined modes are described in the following table.

| Configuration / Predefined Config ID |                  Description                     | Button Presses (or Downlink DefinedConfig id)| Event ID's|
|:-------------:|:-------------------------------------------------|:------------------------------------------:|:----------:|
|   Vibration Impulse Detector / 1  |  Sends notification whenever a vibration or rotation is detected. Waits three minutes to clear between additional notifications. Battery health check interval 3 hours. This configuration is good to monitor any movement such as breaking glass, package tampering, opening a drawer, or safe. <br><br> The device ships with this configuration as the default. Battery life is typically 2+ years.    |       1      | 1 = Impulse Event |
|   Continuous Motion Sensor / 2  | Monitors Continuous Motion. Sends notification when motion begins and 10 seconds after motion ceases. Battery health check interval 3 hours. <br><br> This configuration is useful for monitoring the use of mechanical equipment. Battery life is typically 2+ years. |       2      | 1 = Motion Detected Event <br> 2 = Motion Stopped Event |
|   Garage Door Tilt Detector (Factory DEFAULT) / 3   | Monitors vibration and vertical rotation when attached to a garage door. Sends notifications when opening or closing. Battery health check interval 3 hours. <br> <br> Battery life is typically 2+ years  |       3      | 1 = Garage Door Closed Event <br> 2 = Garage Door Open Event <br> 3 = Garage Door In-Motion Event |
|   Garage Door Tilt Detector (15 minute door open reminder) / 4   | Monitors vibration and vertical rotation when attached to a garage door. Sends notifications when opening or closing. Sends door-open reminder notification  every 15 minutes.  Battery health check interval 3 hours. <br> <br> Battery life is typically 2+ years  |       4      | 1 = Garage Door Closed Event <br> 2 = Garage Door Open Event <br> 3 = Garage Door In-Motion Event <br> 4 = Door Open Reminder Event |
|   Garage Door Tilt Detector (1 hour door open reminder) / 5   | Monitors vibration and vertical rotation when attached to a garage door. Sends notifications when opening or closing. Sends door-open reminder notification every 1 hour.  Battery health check interval 3 hours. <br> <br> Battery life is typically 2+ years  |       5      | 1 = Garage Door Closed Event <br> 2 = Garage Door Open Event <br> 3 = Garage Door In-Motion Event <br> 4 = Door Open Reminder Event |
|   Garage Door Tilt Detector (3 hour door open reminder) / 6   | Monitors vibration and vertical rotation when attached to a garage door. Sends notifications when opening or closing. Sends door-open reminder notification every 3 hours.  Battery health check interval 3 hours. <br> <br> Battery life is typically 2+ years  |       6      | 1 = Garage Door Closed Event <br> 2 = Garage Door Open Event <br> 3 = Garage Door In-Motion Event <br> 4 = Door Open Reminder Event |

### [Temperature Sensor (temperature.proto)](defs/protobuf/temperature.proto)

The CS1040-UL has a coarse resolution (~1.6 C) calibrated temperature sensor useful for monitoring significant changes in temperature.  The sensor provides functions
for interval reporting, absolute/relative triggered threshold notifications, or statistics.   The table below shows the LoRaWAN port mappings for the Temperature service
interactions.  A maximum of six (6) triggers can be configured see performance note below for further limitations.


| Port (Hex) | Port (Dec.) | Interaction Type | Name (Temperature.Sensor.) | Description                               |
|:----------:|:-----------:|:----------------:|:--------------------------:|-------------------------------------------|
|    0x30    |      48     |       Event      |     TriggerNotifyEvent     | Notifies of configured trigger events.    |
|    0x31    |      49     |       Event      |         ReportEvent        | Reports temperature at regular intervals. |
|    0x32    |      50     |       Event      |      StatsReportEvent      | Statistics reported at regular intervals. |
|    0x33    |      51     | Request/Response |          GetConfig         | Gets the sensor configuration.            |
|    0x34    |      52     | Request/Response |          SetConfig         | Sets the sensor configuration.            |
|    0x35    |      53     | Request/Response |          GetStats          | Gets the temperature statistics.          |
|    0x36    |      54     | Request/Response |         StatsClear         | Clears the temperature statistics.        |

#### Peformance Note
Configurations with more than three (3) triggers defined will exceed the minimum datarate (for U.S. it is DR1) payload size restrictions of 51 bytes.
In these cases, the GetConfig/SetConfig responses will not uplink. The only option is to get closer to a gateway so it will communicate at 
at higher data rate.   The maximum size of the configuration can be up to 85 bytes so take that into account when planning deployment.  
If low data rate deployments are required, it is recommended that a  maximum of three (3) triggers be used.   This will allow the
configurationto be packed into the 51 byte message in most cases.

## Example Downlinks

Sample downlinks for configuring the various leak sensor services are available in the postman collection found here
[CS1040-UL-TTNv3-Test.postman_collection.json](test/postman/CS1040-UL-TTNv3-Test.postman_collection.json).  Be sure to look at the documentation 
for each postman they provide additional information about the payload specification as well as the response.

The postmans are set up to send downlinks via TTN V3.0.  The authentication and device ID will have to be updated for 
particular CS1040-UL device to send.   

To create a custom payload:

1) Generate the encoded protobuf and convert to base-64 encoding.
2) Paste into the payload field of the appropriate postman. Recommend duplicating the postman before modifying.
3) Send the message.  
4) Watching the TTN console, you can see the downlink waiting to go and then monitor the responses.

Similar functions are also available with other network providers.

## Tools and Sample Code

 -- Work in progress, check back later --.


### Note regarding Helium and "port-only" / 0-byte downlinks:
On Helium, 0 byte downlinks (i.e. port-only downlinks) are currently discarded by the network.  While this may change in the future, as of 3/2022 on Helium...any 0-byte Downlinks must be padded with at leask 1 junk byte to allow network to forward downlinks to end device.  For any "port-only" downlink examples below, be aware of this consideration.

### Sample Application Protobuf Messages
| Message<br/>| Port <br/> | Request <br/> (Downlink B64) | Response <br/>(Uplink B64) |Description                                                                                     |
|------------------------|--------------------------|--------------------------|--------------------------|-------------------------------------------------------------------------------------------------|
| Health Check Event | 16 | | CEsQKA== | Periodic Health Check Event. Battery 75%.  Temperature 20 C.
| Device Reset Event | 17 | | CAISCHYxLjAuMC41 | Factory Reset Event, f/w Version "v1.0.0.5"
| Device Reset Event | 17 | | CAESCHYwLjYuMC4w | Network Reset Event, f/w Version "v0.6.0.0"
| Get Device Config | 18 | | CAIQoAsaCHYwLjYuMC4w | Gets the current configuration of Device 0. (LED Display Mode: ALL, HealthCheck Interval: 1440 minutes, Version: "v0.6.0.0")|
| Motion Event | 39 | | CAAQARgBIA8= | | Sensor Id 0 Motion Event.  Mode: 3 (Garage Door) Event Id: 1, IdState : 1, Last State Duration: 15 seconds
| Motion Event | 39 | | CAAQAhgDIAQ= | | Sensor Id 0 Motion Event.  Mode: 3 (Garage Door) Event Id: 2, IdState : 3, Last State Duration: 4 seconds
| Motion Event | 39 | | CAAQARgFIAQ= | | Sensor Id 0 Motion Event.  Mode: 3 (Garage Door) Event Id: 1, IdState : 5, Last State Duration: 4 seconds


## Integrator's Guide
This section is for integrator's adapting CS1040 devices to their application.  The required set of messages that must be supported will vary from application to application.  This section outlines the minimum integration and minimum configuration capabilities for baseline use.

### Minimum Integration
  Bare minimum integration requires support of the following messages.  These messages provide the required interaction to receive device notifications and minimum status (connectivity state and battery).

| Port | Direction | Name | Description |
|---|:---:|:---:|---|
| 16 (0x10) | Uplink | Health Check Event | Periodic uplink from device indicating network connectivity (hearbeat).  This message includes Battery Status information.  CS1040 default Health Check interval is 3 hours. |
| 17 (0x11) | Uplink | Reset Event | Message sent whenever the device is reset. Whenever this is received, the adaptor can read the current device configuration to determine if the user has changed the opreating mode and update UI appropriatly if supported (see Get Device Config prior).  Otherwise, the app should reset (assert) the Device Config to a known state (see ResetConfig below).   The Reset Event is also possible if the user replaced the battery's or performed a Network Reset or Factory Reset operation. 
| 39 (0x27) | Uplink | Motion Notify Event | Message sent whenever a motion event occurs.  See notes below.  |

### Minimum Configuration Capability

For basic use of the device, users need to be able to configure the health check interval and set the operating mode (3 standard modes are supported per the datasheet: Impulse, Garage Door, and Continous Vibration).
Advanced motion triggers and custom state-definitions are considered out of scope for basic/minimum integration use.
 However these features provide capabilities for customized use of the device.  The integrator should also consider batch configuration scenario to set the default configuration of many CS1040 devices simultaneously.

| Port | Direction | Name | Description |
|---|:---:|:---:|---|
| 8 (0x8) | Downlink | Set Defined Config | Resets the Device Configuration to EMPTY (0), Vibration Impulse (1), Continuous Motion (2), or Garage Door (3).  Device shall NETWORK RESET after setting the defined configuration.|
| 18 (0x12) | Uplink/Downlink | Get Device Config | Device configuration request / response command allows retrieval of current device configuration.  Adaptor should save the configuration state once received. |
| 19 (0x13) | Downlink | Set Device Config | Sets the device configuration.  Use this to set the device configuration, minimum implementation will allow specification of the health check interval. |


#### Using a Pre-Defined Configuration
CS1040 provides six different operating modes method for Impulse, Continuous Vibration, and Garage Door configurations.  These modes effectivly configure the device with preloaded Motion profile, Trigger Thresholds, and reporting intervals depending on the mode.  The integrators application should implement
an appropriate UI for selecting a mode, as well as interpreting port 39 uplink notifications from the device.  The current device predefined configuration is indicated in the ResetInfo uplink whenever the device restarts. 
Reference [Common.proto](defs/protobuf/common.proto) for details about the Reset Info Notification.

Your application should maintain the state of the pre-defined device mode (Impulse, Continuous, Garage-Door) in order to correctly interpret motion Notify Events on port 39.
The Notify Event's IdEvent field is specific to the current pre-defined configuration.  Reference the CS1040 Standard Configuration Modes and Event Types section for Event Id's applicable to each mode.

#### Pre-Defined Configuration Descriptions
The Impulse Detector mode is designed to send a single uplink whenever an impulse is detected (>200mG), and re-arms after 30 seconds.
<br><br>The Continuous Motion mode sends an notification uplink when motion begins (>200mG) and waits until motion ceases for greater than 10 seconds before sending a motion-stopped notification.
<br><br>The Garage Door Tilt Detector modes are all identical except for the door-open feature.  These modes send an initial uplink when garage door motions starts.  After motion ceases, the corresponding door-open/door-close uplink is sent.
If the garage door is opened and closed in one continous motion, only a motion start event id (ID 1) will be recieved, followed by the final orientation of the door.  You should accomodate this use-case in your application UI design.

#### Example UI Implementation Table
| **Mode** | **Mode Name**     | **Port** | **Event Id** |   **UI Indication**  |   **UI Image**   |      **UI Alert**     | **Comments**                                                                              |
|:--------:|-------------------|:--------:|:------------:|:--------------------:|:----------------:|:---------------------:|-------------------------------------------------------------------------------------------|
|     1    | Impulse           |          |              |      No Activity     |  Device at rest  |                       | UI Startup Default                                                                        |
|     1    | Impulse           |    39    |       1      |    Impulse Deteced   |  Device  shaking | Default Trigger Alert | Show Device Shaking Icon for 30 seconds by default, then return to Device   at Rest state |
|          |                   |          |              |                      |                  |                       |                                                                                           |
|     2    | Continuous Motion |    39    |       1      |    Motion Started    |  Device shaking  | Default Trigger Alert |                                                                                           |
|     2    | Continuous Motion |    39    |       1      |    Motion Stopped    |  Device at rest  | Default Trigger Alert | UI Startup Defaults to motion stopped state                                               |
|          |                   |          |              |                      |                  |                       |                                                                                           |
|     3    | Garage            |    39    |       1      |     Garage Closed    |   Garage Closed  | Default Trigger Alert | UI Startup Defaults to garage closed state                                                |
|     3    | Garage            |    39    |       2      |      Garage Open     |    Garage Open   | Default Trigger Alert |                                                                                           |
|     3    | Garage            |    39    |       3      |   Garage In-Motion   | Garage In Motion |                       |                                                                                           |
|          |                   |          |              |                      |                  |                       |                                                                                           |
|     4    | Garage 15 minute  |    39    |       1      |     Garage Closed    |   Garage Closed  | Default Trigger Alert | UI Startup Defaults to garage closed state                                                |
|     4    | Garage 15 minute  |    39    |       2      |      Garage Open     |    Garage Open   | Default Trigger Alert |                                                                                           |
|     4    | Garage 15 minute  |    39    |       3      |   Garage In-Motion   | Garage In Motion |                       |                                                                                           |
|     4    | Garage 15 minute  |    39    |       4      | Garage Open Reminder |    Garage Open   | Default Trigger Alert |                                                                                           |
|          |                   |          |              |                      |                  |                       |                                                                                           |
|     5    | Garage 1 hour     |    39    |       1      |     Garage Closed    |   Garage Closed  | Default Trigger Alert | UI Startup Defaults to garage closed state                                                |
|     5    | Garage 1 hour     |    39    |       2      |      Garage Open     |    Garage Open   | Default Trigger Alert |                                                                                           |
|     5    | Garage 1 hour     |    39    |       3      |   Garage In-Motion   | Garage In Motion |                       |                                                                                           |
|     5    | Garage 1 hour     |    39    |       4      | Garage Open Reminder |    Garage Open   | Default Trigger Alert |                                                                                           |
|          |                   |          |              |                      |                  |                       |                                                                                           |
|     6    | Garage 3 hour     |    39    |       1      |     Garage Closed    |   Garage Closed  | Default Trigger Alert | UI Startup Defaults to garage closed state                                                |
|     6    | Garage 3 hour     |    39    |       2      |      Garage Open     |    Garage Open   | Default Trigger Alert |                                                                                           |
|     6    | Garage 3 hour     |    39    |       3      |   Garage In-Motion   | Garage In Motion |                       |                                                                                           |
|     6    | Garage 3 hour     |    39    |       4      | Garage Open Reminder |    Garage Open   | Default Trigger Alert |                                                                                           |

### Advanced Configuration
The CS1040 supports custom motion sensitivty, range, and threshold triggers based on a 6-axis Inertial Measurement Unit (Acceleration + Gyro).  
State Definitions and Triggers must be designed to configure the device in a custom mode (see motion.proto).  Contact Codepoint for 
assistance with application development for custom motion, vibration, and tilt applications.

## License

Copyright 2022 Codepoint Technologies, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO 
EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
USE OR OTHER DEALINGS IN THE SOFTWARE.
