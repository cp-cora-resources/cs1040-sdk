/*
 * Copyright (c) 2021, Codepoint Technologies, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
*  "uripkg": "//hub.cpflex.tech/ocm/ocs/cs1000_ul",
*    "tag": "v1.0",
*    "title": "Specification for CS1000_UL Untethered LoRaWAN Protocol.",
*    "dateCreated": "2021-11-24T04:15:40:00.000Z",
*    "dateModified": "${__ISODATE__}",
*/

syntax = "proto2";
import "lowPowerDevice.proto";
import "motion.proto";
import "temperature.proto";

package Cora.CS1040;

/**
* Specifies the configuration modes providing custom or predefined configurations.
*/
enum ConfigurationMode {
    option allow_alias = true;
    CLEAR=0;                //Clears any existing triggers and state definitions.
    CUSTOM=0;               //Sensor is configured with custom states and triggers.
    IMPULSE=1;             //Device will notify whenever a vibration impulse is detected after being idle for 30 seconds or more.
    CONTINUOUS=2;          //Device will notify start and stop of continuous vibration (longer than 10 seconds).
    GARAGE0=3;            //Notifies when garage is open or closed. No reminder.
    GARAGE1=4;            //Notifies when garage is open or closed. Sends reminder when open for more than 1 hour.
    GARAGE3=5;            //Notifies when garage is open or closed. Sends reminder when open for more than 3 hours.
}