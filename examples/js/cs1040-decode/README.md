# CS1040 CLI Decoder Example 
This javascript command line application demonstrates how to decode CS1040 messages protobufjs library.

## Setup
1. Open command line in this folder and execute the following commands
2. Pull dependent modules using NPM
   ```
   npm install
   ```
3. Verify application is working.
   ```
   node cs1040-decode --help
   ```
   This should produce:

   ```
    Usage: cs1040-decode [options] <port> <msg>

    Example to demonstrate decoding base-64 encoded messages from the CS-1040

    Arguments:
    port        The uplink port message was received
    msg         The base-64 encoded message to decode.

    Options:
    -h, --help  display help for command
   ```
4. Try decoding uplinks.  This requires a base64 encoded payload from the device and the port number it received.  For example:

   * Decode event message.

   ```json
    > node .\cs1040-decode 39 CAAQARgBIA8=   
    {
    "id": 0,
    "idEvent": 1,
    "idState": 1,
    "dtLast": 15
    }
   ```

   * Decode Config
   ```json
   > node .\cs1040-decode 18 CAIQoAsaCHYwLjYuMC4w
   {
    "displayMode": "ALL",
    "healthCheckInterval": 1440,
    "version": "v0.6.0.0"
   }
   ```

   ## Implementing your own decoder
   Implementing your own decoder requires the two json files: *cs1040-portmap-v1.0.json* and *cs1040-protobuf-v1.0.json*.  These files contain 
   the port mappings and protobuf definitions for decoding or encoding CS1040 messages for uplink or downlink.

   Decoding uplinks is as follows:

   ```javascript

    const portmap = require("./cs1040-portmap-v1.0.json");
    const proto = require("./cs1040-protobuf-v1.0.json");
    const protobuf = require("protobufjs");

    //... 
   
    /**
    * Decodes CS1040 uplink message.
    * @param {number} port Uplink port number.
    * @param {string} msg  Base 64 encoded binary message.
    * @returns {string} The uplink as JSON
    */
    function decodeUplinkCS1040Uplink(port, msg) {
        //Map port number to protobuf message.
        const pm = portmap.find((item) => item.port == port);
        if (pm) {
            //create protobuf decoder for the specific uplink
            const root = protobuf.Root.fromJSON(proto);
            const decoder = root.lookupType(pm.uplk);

            //decode the encoded payload.
            const buff = Buffer.from(msg, "base64");
            const decoded_payload = decoder.decode(buff);

            //Output the decoded payload as JSON.
            return JSON.stringify(decoded_payload, null, 2);
        }
    }
   
   ```