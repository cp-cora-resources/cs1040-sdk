#! /user/bin/env node
/*
 *  Name:  cs1040-decode.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 202r Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const { program } = require("commander");
const portmap = require("./cs1040-portmap-v1.0.json");
const proto = require("./cs1040-protobuf-v1.0.json");
const protobuf = require("protobufjs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "Example to demonstrate decoding base-64 encoded messages from the CS-1040",
      {
        port: "The uplink port message was received",
        msg: "The base-64 encoded message to decode.",
      }
    )
    .arguments("<port> <msg>")

    //------------------------------------------------------------------------
    // Command implementation.
    //------------------------------------------------------------------------
    .action(function (port, msg) {
      //Decode the uplink
      const json = decodeUplinkCS1040Uplink(port, msg);

      //Display the decoded payload as JSON.
      console.info(json);
    });
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}

//------------------------------------------------------------------------
// Codec implementation.
//------------------------------------------------------------------------

/**
 * Decodes CS1040 uplink message.
 * @param {number} port Uplink port number.
 * @param {string} msg  Base 64 encoded binary message.
 * @returns {string} The uplink as JSON
 */
function decodeUplinkCS1040Uplink(port, msg) {
  //Map port number to protobuf message.
  const pm = portmap.find((item) => item.port == port);
  if (pm) {
    //create protobuf decoder for the specific uplink
    const root = protobuf.Root.fromJSON(proto);
    const decoder = root.lookupType(pm.uplk);

    //decode the encoded payload.
    const buff = Buffer.from(msg, "base64");
    const decoded_payload = decoder.decode(buff);

    //Output the decoded payload as JSON.
    return JSON.stringify(decoded_payload, null, 2);
  }
}
